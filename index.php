<!DOCTYPE html>
<?php 
  require 'vendor/autoload.php';

  $assets["bootstrap"] = "bower_components/bootstrap/dist/"; 
  $assets["jquery"] = "bower_components/jquery/";
  

  $config = array(
      'appId' => '765767246772090',
      'secret' => '2124dbe68bc480f277b846c4a0e586b0',
      'fileUpload' => false, // optional
      'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
  );

  $facebook = new Facebook($config);
  $userId = $facebook->getUser();
  if($userId){
    try {
      $events = $facebook->api(
        "/313436648772666/events?fields=id,name,picture,start_time,attending"
      );
           
      foreach ($events['data'] as &$event) {        
        
        $date = DateTime::createFromFormat(DateTime::ISO8601,$event['start_time']);          
        if(!$date)
          $date  = DateTime::createFromFormat('Y-m-d',$event['start_time']);           
        $event['start_time'] = $date->format('d-M-Y');         
        
        $event['cover'] = $event['picture']['data']['url'];
        $event['attendence_count'] = count($event['attending']['data']);
      }
      
    } catch (FacebookApiExcveption $e) {
      $loginUrl = $facebook->getLoginUrl();      
    }
  }
  else{
    $loginUrl = $facebook->getLoginUrl();    
  }
   
?>
<html>
  <head>
    <title>CSI: URA Prezente App</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?=$assets["bootstrap"]?>css/bootstrap.min.css" rel="stylesheet">

    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <?php if(isset($loginUrl)): ?>
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <a href="<?=$loginUrl?>" class="btn btn-primary">Login to Facebook</a>
          </div>
        </div>
      </div>
    <?php else: ?>
      <div class="container">
        <div class="row">
          <div class="col-lg-4 side">
            <?php if(isset($events)): ?>
              <?php foreach ($events['data'] as $event): ?> 
                <div class="event">
                  <div class="media">
                    <a class="pull-left" href="<?=$event['id'];?>">
                      <img class="media-object" src="<?=$event['cover'];?>" alt="event cover">
                    </a>
                    <div class="media-body">
                      <h4 class="media-heading"><?=$event['name'];?></h4>
                      <span class=""><?=$event['start_time']?></span><br>
                      <span class="">Participanti: <?=$event['attendence_count']?></span><br>
                      <span class=""><a href="#" class="btn btn-default btn-sm prezenta">Prezenta</a></span>
                    </div>
                  </div>                
                  <div class="profile clearfix">
                    <?php foreach ($event['attending']['data'] as $participant): ?>
                      <a href="#" title="<?=$participant['name'];?>" class="attendee">
                        <img src="https://graph.facebook.com/<?=$participant['id']?>/picture?height=100" title="<?=$participant['name'];?>">                      
                      </a>
                    <?php endforeach; ?>
                  </div>
                </div>
                <hr>
              <?php endforeach; ?>
            <?php endif; ?>    
          </div>
          <div class="col-lg-8 content">
            <div class="profile">
              
            </div>
          </div>
        </div>
      </div>      
    <?php endif; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=$assets["jquery"]?>jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=$assets["bootstrap"]?>js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>